#ifndef LOADTHREAD_H
#define LOADTHREAD_H

#include <QObject>
#include <QRunnable>
#include <QString>
#include <QThread>
#include <QDebug>
#include <string>
#include <iostream>

#include "mediadata.h"


#include <ZenLib/Ztring.h>
#include <MediaInfo/MediaInfo.h>

using namespace MediaInfoLib;
using namespace ZenLib;



class LoadThread :  public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit LoadThread();
    void run();

public:
    void setFilename(QString fn);
    QString stringToCamelCase(QString str);

signals:
    void fileData(MediaData *data);
private:
    QString currentFilename;
};


#endif // LOADTHREAD_H
