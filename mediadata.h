#ifndef MEDIADATA_H
#define MEDIADATA_H

#include <QString>

#endif // MEDIADATA_H


class MediaData
{
public:
    QString path = "";
    bool exist = false;
    bool error = false;
    QString optionSummary ="";
    QMap<QString, QString> options;
};
