#include "fileinfolist.h"


FileInfoList::FileInfoList(QWidget *parent)
    : QWidget(parent){

    //Fix async constructor connection
    QTimer::singleShot(10, this, SLOT(readDB()));
}
void FileInfoList::readDB(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("info.denisow.desktop.qMediaInfoList.db");
    if (!db.open()){
        qDebug() << "Error: connection with database fail";
    }else{
        QSqlQueryModel model;
        model.setQuery("CREATE TABLE IF NOT EXISTS files(id integer primary key, path text, error boolean, exist boolean, AudioBitRate text, AudioBitRateMode text, AudioChannels text, AudioCodecID text, AudioCompressionMode text, AudioDuration text, AudioEncodedDate text, AudioFormat text, AudioFormatInfo text, AudioFormatProfile text, AudioFrameRate text, AudioID text, AudioMaximumBitRate text, AudioSamplingRate text, AudioStreamSize text, AudioTaggedDate text, AudioTitle text, GeneralCodecID text, GeneralCompleteName text, GeneralDuration text, GeneralEncodedDate text, GeneralFileSize text, GeneralFormat text, GeneralFormatProfile text, GeneralOverallBitRate text, GeneralOverallBitRateMode text, GeneralTaggedDate text, VideoBitDepth text, VideoBitRate text, VideoBitsPixelFrame text, VideoChromaSubsampling text, VideoCodecID text, VideoCodecIDInfo text, VideoColorSpace text, VideoDisplayAspectRatio text, VideoDuration text, VideoEncodedDate text, VideoFormat text, VideoFormatInfo text, VideoFormatProfile text, VideoFormatSettingsCABAC text, VideoFormatSettingsGOP text, VideoFormatSettingsReFrames text, VideoFrameRate text, VideoFrameRateMode text, VideoHeight text, VideoID text, VideoMaximumBitRate text, VideoScanType text, VideoStreamSize text, VideoTaggedDate text, VideoWidth text);");
        if (model.lastError().isValid()){
            qDebug() << model.lastError();
        }
        model.setQuery("PRAGMA table_info(files);");
        for(int i = 0; i < model.rowCount(); i++)
        {
            options << model.record(i).value(1).toString();
        }
        model.setQuery("SELECT * FROM files ORDER BY ID DESC LIMIT 100;");
        for(int i = model.rowCount(); i >= 0; i--)
        {
            MediaData *data = new MediaData;
            data->path = model.record(i).value(1).toString();
            data->error = (model.record(i).value(2).toString()  == "1");
            data->exist = (model.record(i).value(3).toString()  == "1");
            for(int j = 4; j < options.size(); ++j){
                data->options.insert(options[j], model.record(i).value(j).toString());
            }

//           qDebug() << " LOAD ==>" << data->path << data->options["GeneralFormat"];
           emit newFileData(data);
        }
    }
}

void FileInfoList::getFile(const QString &filename)
{
    LoadThread *LoadWorker = new LoadThread();
    LoadWorker->setFilename(filename);
    QObject::connect(LoadWorker, &LoadThread::fileData,
                           this, &FileInfoList::updateFileData);
    QThreadPool::globalInstance()->start(LoadWorker);
}
void FileInfoList::getFileList(QStringList list){
    for (int i = 0; i < list.size(); ++i){
        this->getFile(list[i]);
    }
}

void FileInfoList::test(){
    QStringList list;
    list << "notExist.pm4"
         << "../../../../qMediaInfoList/test-files/test1.mp4"
         << "../../../../qMediaInfoList/test-files/test2.mp4"
         << "../../../../qMediaInfoList/test-files/test3.mp4"
         << "../../../../qMediaInfoList/test-files/test4 \"한국어 .mp4"
         << "../../../../qMediaInfoList/test-files/עִבְרִית မြန်မာစာالعَرَبِيَّة‎‎/test5.mp4";
    this->getFileList(list);
}

void FileInfoList::updateFileData(MediaData *data){
    //qDebug() << " READ ==>" << data->path << data->options["GeneralFormat"];
    emit newFileData(data);

    QString optionsQueryKeys = "";
    QString optionsQueryValues = "";
    for(int i=0;i<data->options.size();i++){
        QString key = data->options.keys()[i];
        if(key!="" && data->options[key]!="" && options.indexOf(key)>=0){
        optionsQueryKeys += ","+key+" ";
        optionsQueryValues += ", '"+data->options[key]+"'";
        }
    }

    QString sQuery = "INSERT INTO files (path,error,exist"+optionsQueryKeys+") VALUES ('"+data->path+"', '"+(data->error ? "1" : "0")+"', '"+(data->exist ? "1" : "0")+"'"+optionsQueryValues+");";
    QSqlQueryModel model;
    model.setQuery(sQuery);
    if (model.lastError().isValid()){
        qDebug() << model.lastError();
    }


}

FileInfoList::~FileInfoList()
{
}
