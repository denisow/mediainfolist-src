#include "loadthread.h"
#include <QTimer>
#include <QFileInfo>

LoadThread::LoadThread() : QObject()
{
    currentFilename = "";
}

void LoadThread::setFilename(QString fn){
  currentFilename = fn;
}

void LoadThread::run(){
   MediaData *data = new MediaData;
   QFileInfo info(currentFilename);
   data->path = info.absoluteFilePath();
   data->exist = info.exists();
   if(data->exist){
       MediaInfo *MI = new MediaInfo();
       MI->Open(data->path.toStdWString());
       MI->Option(__T("Complete"));
       data->optionSummary = QString::fromStdWString(MI->Inform());
       MI->Close();

       QStringList optionRows = data->optionSummary.split('\n');
       QString prefix;
       for (int i = 0; i < optionRows.size(); ++i){
            QStringList row = optionRows.at(i).split(": ");
            if(row.size() == 1 && row[0]!=""){
                prefix = row[0];
            }else if(row.size() == 2) {
                data->options.insert(this->stringToCamelCase(prefix+row[0]), row[1]);
           }
       }
   }else{
       data->error = true;
   }
   if(data->options["GeneralCompleteName"] == ""){
       data->error = true;
   }
   emit fileData(data);
}

QString LoadThread::stringToCamelCase(QString str){
    QString key = str.remove(QRegExp("[^a-zA-Z\\d\\s]"));
    QStringList parts = key.split(' ', QString::SkipEmptyParts);
    for (int i=1; i<parts.size(); ++i){
        parts[i].replace(0, 1, parts[i][0].toUpper());
    }
    key = parts.join("");
    return key;
}
