#ifndef FILEINFOLIST_H
#define FILEINFOLIST_H

#include <QObject>
#include <QFileInfo>
#include <QDebug>
#include <QThreadPool>
#include <QWidget>
#include <QApplication>
#include <QtSql>

#include "loadthread.h"


class FileInfoList : public QWidget
{

    Q_OBJECT

public:
    FileInfoList(QWidget *parent);
    ~FileInfoList();

public slots:

    void getFile(const QString &filename);
    void getFileList(QStringList list);
    void test();
    void updateFileData(MediaData *data);
    void readDB();

signals:
    void newFileData(MediaData *data);

private:
    QList<QFileInfo*> fileInfoList;
    QStringList options;

};

#endif // FILEINFOLIST_H
