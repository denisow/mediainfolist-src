#include <QFileDialog>
#include <QBuffer>
#include "mainwindow.h"




MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent)
{

    setAcceptDrops(true);
    this->infoList = new FileInfoList(this);
    QObject::connect(this->infoList, &FileInfoList::newFileData,
                               this, &MainWindow::newCard);



    QScrollArea* cardsArea = new QScrollArea();
    QWidget* cardsSurface = new QWidget();
    this->cardsLayout = new QVBoxLayout;
    this->cardsLayout->setSizeConstraint(QLayout::SetMinimumSize);
    this->cardsLayout->setDirection(QBoxLayout::BottomToTop);
    cardsSurface->setLayout(this->cardsLayout);
    cardsSurface->resize(QSize(350, 1000));
    cardsArea->setWidget(cardsSurface);

    this->fileDropArea = new QPushButton(tr("Click to load new file, or drag and drop"),this);
    QObject::connect(this->fileDropArea, &QPushButton::clicked,
                               this, &MainWindow::chooseFile);


    QVBoxLayout *layout = new QVBoxLayout;
        layout->addWidget(cardsArea);
        layout->addWidget(this->fileDropArea);

    this->setLayout(layout);
    this->setStyleSheet("QWidget {background-color: white;}");
    cardsArea->setStyleSheet("QScrollArea {border: none; min-height: 450px; min-width: 520px;}");
    this->fileDropArea->setStyleSheet("QPushButton {font-size: 22px; min-height: 150px; min-width: 520px; border: 5px dashed #2584b8;  border-radius: 10px}; QPushButton:hover { border: 6px dashed #ff84b8;}");


}


void MainWindow::newCard(MediaData *data){


    QFileInfo  fileInfo(data->path);


    QString cardText = "<p><font font size = 4>";
    cardText .append( fileInfo.fileName()+"<br/>");
    cardText .append("<font font size = 2>");
    cardText .append("<br/>");
    cardText .append(data->options["GeneralFormat"]+" "+data->options["GeneralFileSize"]+" "+data->options["GeneralDuration"]+"<br/><br/>");
    if(!data->error){
        cardText .append(tr("Video")+": "+data->options["VideoFormatSettingsReFrames"]+" "+data->options["VideoFormat"]+" "+data->options["VideoBitRate"]+" "+data->options["VideoBitDepth"]+"<br/>");
        cardText .append(""+data->options["VideoDuration"]+" "+data->options["VideoFrameRate"]+" "+data->options["VideoDisplayAspectRatio"]+" "+data->options["VideoWidth"]+" X "+data->options["VideoHeight"]+"<br/><br/>");
        cardText .append(tr("Audio")+": "+data->options["AudioChannels"]+" "+data->options["AudioFormat"]+" "+data->options["AudioBitRate"]+" "+data->options["AudioSamplingRate"]+"<br/>");
    }
    cardText .append(""+data->options["AudioDuration"]+" "+data->options["AudioStreamSize"]+" "+data->options["AudioTitle"]+"<br/>");
    cardText .append("</font></p>");

    QLabel* card = new QLabel();
    card->setText(cardText);
    QString customQSS = "";
    if(data->error){
        customQSS = "background-color: #EEE;";
    }else if(!data->exist){
        customQSS = "background-color: #FDD;";
     }
    card->setStyleSheet("QLabel {min-height: 150px; border-bottom: 1px solid #CCC;"+customQSS+"}");
    card->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    cards.append(card);
    this->cardsLayout->addWidget(cards.last());

}

void MainWindow::chooseFile(){
    QStringList fileName = QFileDialog::getOpenFileNames(this, tr("Load File"),
                                                    QDir::homePath(),
                                                    tr("Video (*.3g2 *.3gp *.amv *.asf *.avi *.drc *.flv *.flv *.flv *.f4v *.f4p *.f4a *.f4b *.gif *.gifv *.m4v *.mkv *.mng *.mov *.qt *.mp4 *.m4p *.m4v *.mpg *.mp2 *.mpeg *.mpe *.mpv *.mpg *.mpeg *.m2v *.mxf *.nsv *.ogv *.ogg *.rm *.rmvb *.roq *.svi *.vob *.webm *.wmv *.yuv)"));
    this->infoList->getFileList(fileName);
}

void MainWindow::dropEvent(QDropEvent *ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();
    foreach(QUrl url, urls){
        this->infoList->getFile( url.path() );
    }
}
void MainWindow::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->accept();
}

MainWindow::~MainWindow()
{
}
