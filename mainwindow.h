#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QDebug>
#include <QLabel>
#include <QDropEvent>
#include <QUrl>

#include "loadthread.h"
#include "fileinfolist.h"



class MainWindow;

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QPushButton *fileDropArea;
    QVBoxLayout *cardsLayout;


public slots:
    void newCard(MediaData *data);
    void chooseFile();

private:
    FileInfoList *infoList;
    QList< QLabel*> cards;

protected:
    void dropEvent(QDropEvent *ev);
    void dragEnterEvent(QDragEnterEvent *ev);
};

#endif // MAINWINDOW_H
