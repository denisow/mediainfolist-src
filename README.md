# qMediaInfoList
version 1.0

QT App for display media file meta information using MediaInfo SDK



## Before build

### Mac

#### ! IMPORTANT !
The flag
```
	QMAKE_CXXFLAGS += -DUNICODE
```
SHOULD BE in .pro file




Besides [QT](https://www.qt.io) You need MediaInfo SDK:

```sh
	brew install mediainfo
```

Package will be installed in this folder
```sh
	/usr/local/opt/media-info/
```



## TODO

* Fix not latin filename BUG
* Add card wotget insted on QLabel 